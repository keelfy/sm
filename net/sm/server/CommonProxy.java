package net.sm.server;

import net.sm.registry.module.EventModule;

/**
 * Created by keelfy on 18.07.2016.
 */
public class CommonProxy
 {
   public void initMod() {}
   
   public void registerEvents() {
	   EventModule.init();
   }
   
   public void registerGui() {
	   
   }
 }
