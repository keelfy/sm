package net.sm.server;

import java.util.ArrayList;
import java.util.Iterator;

import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.entity.monster.EntitySlime;
import net.minecraft.entity.monster.EntitySpider;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.entity.passive.EntityCow;
import net.minecraft.entity.passive.EntityHorse;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.entity.passive.EntitySheep;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.biome.SpawnListEntry;

public class RemovingEntity {
    public static ArrayList mobsToDelete = new ArrayList();
    public static ArrayList creaturesToDelete = new ArrayList();
    
	public static void init() {
		mobsToDelete.add(EntitySpider.class);
        mobsToDelete.add(EntitySkeleton.class);
        mobsToDelete.add(EntityCreeper.class);
        mobsToDelete.add(EntitySlime.class);
        mobsToDelete.add(EntityEnderman.class);
        creaturesToDelete.add(EntitySheep.class);
        creaturesToDelete.add(EntityPig.class);
        creaturesToDelete.add(EntityChicken.class);
        creaturesToDelete.add(EntityHorse.class);
        creaturesToDelete.add(EntityCow.class);
        mobsToDelete.add(EntityZombie.class);

        BiomeGenBase[] arr$ = BiomeGenBase.biomeList;
        int len$ = arr$.length;

        for(int i$ = 0; i$ < len$; ++i$) {
            BiomeGenBase biome = arr$[i$];
            if(biome != null) {
                Iterator spawns = biome.spawnableMonsterList.iterator();

                while(spawns.hasNext()) {
                    if(mobsToDelete.contains(((SpawnListEntry)spawns.next()).entityClass)) {
                        spawns.remove();
                    }
                }

                spawns = biome.spawnableCreatureList.iterator();

                while(spawns.hasNext()) {
                    if(creaturesToDelete.contains(((SpawnListEntry)spawns.next()).entityClass)) {
                        spawns.remove();
                    }
                }


            }
        }
	}
}
