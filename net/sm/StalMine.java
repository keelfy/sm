package net.sm;

import java.util.logging.Level;
import java.util.logging.Logger;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.client.Minecraft;
import net.minecraftforge.common.MinecraftForge;
import net.sm.block.SMAnomaly;
import net.sm.client.gui.GuiPlayerStats;
import net.sm.client.sound.SoundManager;
import net.sm.packet.SMPacketHandler;
import net.sm.registry.BlockRegister;
import net.sm.registry.ItemRegister;
import net.sm.registry.module.BlockModule;
import net.sm.registry.module.ItemModule;
import net.sm.server.CommonProxy;
import net.sm.server.RemovingEntity;
import net.sm.server.TickHandler;

/**
 * Created by keelfy on 18.07.2016.
 */
@Mod(modid = Constants.MODID, name = Constants.NAME, version = Constants.VERSION)
@NetworkMod(clientSideRequired = true, serverSideRequired = true, channels = {Constants.CHANNEL1}, packetHandler = SMPacketHandler.class)
public class StalMine {

    @Mod.Instance(Constants.MODID)
    public static StalMine instance;

    @SidedProxy(clientSide=Constants.CLIENT, serverSide=Constants.SERVER)
    public static CommonProxy proxy;

    SoundManager sound = new SoundManager();
    public static Logger logger;
    
    @Mod.EventHandler
    public void serverStart(FMLServerStartingEvent event) throws Exception {
    	
    	RemovingEntity.init();
    }


    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
    	logger = event.getModLog();    	
    	logger.log(Level.FINE, "S.T.A.L.M.I.N.E.: Loading started...");
    	logger.log(Level.FINE, "Modifucation S.T.A.L.M.I.N.E. created by keelfy & mercury.");
    	
        ItemRegister.setResourceDomain(Constants.MODID);
        BlockRegister.setResourceDomain(Constants.MODID);
        SMAnomaly.setResourceDomain(Constants.MODID);
        
        ItemModule.init();
        BlockModule.init();
        
        proxy.initMod();
    }
    
    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
    	
        proxy.registerEvents();
        TickRegistry.registerTickHandler(new TickHandler(), Side.SERVER);
    }
    
    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
    	
    	proxy.registerGui();
    	
    	logger.log(Level.FINE, "S.T.A.L.M.I.N.E.: Loading ended.");
    }
}