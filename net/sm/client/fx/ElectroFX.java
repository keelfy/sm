package net.sm.client.fx;

import static org.lwjgl.opengl.GL11.*;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.sm.Constants;

/**
 * Created by keelfy on 24.07.2016.
 */
public class ElectroFX  extends EntityFX {

	private static final ResourceLocation texture = new ResourceLocation(Constants.MODID + "/textures/fx/electrofx.png");
	private static float alpha;
    private static boolean faze;
	
	public ElectroFX(World par1World, double x, double y, double z) {
		super(par1World, x, y, z, 0.0D, 0.0D, 0.0D);
		setGravity(-1);
		setMaxAge(10);
		
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void renderParticle(Tessellator tess, float ticks, float par3, float par4, float par5, float par6, float par7){
		Minecraft.getMinecraft().renderEngine.bindTexture(texture);
		glDepthMask(false);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glAlphaFunc(GL_GREATER, 0.003921569F);
		tess.startDrawingQuads();
		tess.setBrightness(getBrightnessForRender(ticks));
		float scale = 0.2F * particleScale;
		float x = (float)(prevPosX + (posX - prevPosX) * ticks - interpPosX);
	    float y = (float)(prevPosY + (posY - prevPosY) * ticks - interpPosY);
	    float z = (float)(prevPosZ + (posZ - prevPosZ) * ticks - interpPosZ);
		tess.addVertexWithUV(x - par3 * scale - par6 * scale, y - par4 * scale, z - par5 * scale - par7 * scale, 0, 0);
        tess.addVertexWithUV(x - par3 * scale + par6 * scale, y + par4 * scale, z - par5 * scale + par7 * scale, 1, 0);
        tess.addVertexWithUV(x + par3 * scale + par6 * scale, y + par4 * scale, z + par5 * scale + par7 * scale, 1, 1);
        tess.addVertexWithUV(x + par3 * scale - par6 * scale, y - par4 * scale, z + par5 * scale - par7 * scale, 0, 1);
		tess.draw();
		glDisable(GL_BLEND);
		glDepthMask(true);
		glAlphaFunc(GL_GREATER, 0.1F);
	}
	
	@Override
	public void onUpdate() {
	         this.prevPosX = this.posX;
	         this.prevPosY = this.posY;
	         this.prevPosZ = this.posZ;

	         if (this.particleAge++ >= this.particleMaxAge)
	         {
	             this.setDead();
	         }

	         this.motionY -= 0.001D * (double)this.particleGravity;
	         this.moveEntity(this.motionX, this.motionY, this.motionZ);
	         this.motionX *= 0.79999999776482582D;
	         this.motionY *= 0.1D;
	         this.motionZ *= 0.709999999776482582D;

	         if (this.onGround)
	         {
	             this.motionX *= 0.699999988079071D;
	             this.motionZ *= 0.699999988079071D;
	         }
	        
	        if(faze) {
	            if(alpha < 1.0F) {
	               alpha += 0.025F;
	            } else {
	               faze = false;
	               alpha = 1.0F;
	            }
	         } else if(alpha > 0.0F) {
	            alpha -= 0.05F;
	         } else {
	            faze = true;
	            alpha = 0.0F;
	         }
	         glColor4f(1.0F, 1.0F, 1.0F, alpha);
	    }

	
	public int getFXLayer(){
		return 3;
	}
	
	public ElectroFX setMaxAge(int maxAge){
		particleMaxAge = maxAge;
		return this;
	}
	
	public ElectroFX setScale(float scale){
		particleScale = scale;
		return this;
	}
	
	public ElectroFX setGravity(float gravity){
		particleGravity = gravity;
		return this;
	}
}