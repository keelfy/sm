package net.sm.client.gui;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.event.ForgeSubscribe;
import net.sm.Constants;
import net.sm.client.render.SMRenderHelper;
import net.sm.properties.SMPlayer;

/**
 * Created by keelfy on 22.07.2016.
 */
public class GuiPlayerStats extends Gui {
		
	private Minecraft mc;
	private ResourceLocation texPath = Constants.guiPlayerStats;
	
	public GuiPlayerStats(Minecraft minecraft) {
		super();
		mc = minecraft;
	}
	
	@ForgeSubscribe
	public void eventHandler(RenderGameOverlayEvent event) {
		
	if(!mc.thePlayer.capabilities.isCreativeMode) {
		if(event.type == ElementType.BOSSHEALTH)
			event.setCanceled(true);
		else if(event.type == ElementType.HEALTH)
			event.setCanceled(true);
		else if(event.type == ElementType.HEALTHMOUNT)
			event.setCanceled(true);
		else if(event.type == ElementType.ARMOR)
			event.setCanceled(true);
		else if(event.type == ElementType.EXPERIENCE)
			event.setCanceled(true);
		else if(event.type == ElementType.FOOD)
			event.setCanceled(true);
		else if(event.type == ElementType.HOTBAR)
			event.setCanceled(true);
		
		SMPlayer smp = SMPlayer.get(this.mc.thePlayer);
	    final RenderItem itemRenderer = new RenderItem();
		
		int w = event.resolution.getScaledWidth();
		int h = event.resolution.getScaledHeight();
		int sf = event.resolution.getScaleFactor();
		
		int texSizeX = 1024;
		int texSizeY = 1024;
		float scale = 0.15F * sf;
		
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glDisable(GL11.GL_LIGHTING);
		this.mc.getTextureManager().bindTexture(texPath); 
		
				
		//------------------------------HUD------------------------------------------------
		double gPosX = (w - 442 * scale) / scale;
		double gPosY = (h - 245 * scale) / scale;

		GL11.glPushMatrix();
			GL11.glScalef(scale, scale, 1);
			SMRenderHelper.drawTextureCustomSize(gPosX, gPosY, 1, 582, 779, 442, 245, texSizeX, texSizeY);
		GL11.glPopMatrix();
		//---------------------------------------------------------------------------------
		
		
		//------------------------------Health---------------------------------------------
		double hpPosX = (w - 433 * scale) / scale;
		double hpPosY = (h - 86 * scale) / scale;
		int hpBarWidth = (int)(((float) mc.thePlayer.getHealth() / mc.thePlayer.getMaxHealth()) * 276);
			
		GL11.glPushMatrix();
			GL11.glScalef(scale, scale, 1);
			SMRenderHelper.drawTextureCustomSize(hpPosX, hpPosY, 2, 716, 666, hpBarWidth, 32, texSizeX, texSizeY);
		GL11.glPopMatrix();
		//---------------------------------------------------------------------------------
		
		
		//------------------------------Stamina--------------------------------------------
		double stPosX = (w - 433 * scale) / scale;
		double stPosY = (h - 47 * scale) / scale;
		int stBarWidth = (int)(((float) smp.getAdversityInfo(1, 1) / smp.getAdversityInfo(2, 1)) * 276);
			
		GL11.glPushMatrix();
			GL11.glScalef(scale, scale, 1);
			SMRenderHelper.drawTextureCustomSize(stPosX, stPosY, 2, 716, 712, stBarWidth, 32, texSizeX, texSizeY);
		GL11.glPopMatrix();
		//---------------------------------------------------------------------------------

		
		//------------------------------Armor----------------------------------------------
		double armorPosX = (w-250 * scale) / scale;
		double armorPosY = (h-223 * scale) / scale;
		int armorBarHeight = (int)(((float) smp.getDefenseInfo(1, 1) / smp.getDefenseInfo(2, 1)) * 120);

		GL11.glPushMatrix();
			GL11.glScalef(scale, scale, 1);
			GL11.glTranslatef(1f, 1f, 3f);
			GL11.glEnable(GL11.GL_BLEND);
			SMRenderHelper.drawTextureCustomSize(armorPosX, armorPosY, 2, 594, 656, 87, armorBarHeight, texSizeX, texSizeY);
			GL11.glDisable(GL11.GL_BLEND);
		GL11.glPopMatrix();
		//---------------------------------------------------------------------------------
		
		
		//------------------------------Current Item---------------------------------------
	    this.mc.getTextureManager().bindTexture(icons);
	    GL11.glEnable(GL11.GL_BLEND);
	    this.drawTexturedModalRect(w / 2 - 7, h / 2 - 7, 0, 0, 16, 16);
	    
		int count = 0;
		if(mc.thePlayer.getCurrentEquippedItem() != null)
			count = mc.thePlayer.getCurrentEquippedItem().stackSize;
        String s1 = count == 0 ? ""  : String.valueOf(count);
		double s1PosX = w-15;
		double s1PosY = h-21;
		GL11.glPushMatrix();
			GL11.glTranslatef(1f, 1f, 2f);
			this.drawString(mc.fontRenderer, EnumChatFormatting.GRAY + s1, (int)s1PosX - mc.fontRenderer.getStringWidth(s1), (int)s1PosY, -65536);
		GL11.glPopMatrix();
		GL11.glDisable(GL11.GL_BLEND);
	        
		double curItemPosX = ((w - 111) * scale) / scale;
		double curItemPosY = ((h - 58)* scale) / scale;
	    GL11.glEnable(GL12.GL_RESCALE_NORMAL);
	    RenderHelper.enableGUIStandardItemLighting();
	    GL11.glPushMatrix();
		    GL11.glTranslatef(1f, 1f, 2f);
		    SMRenderHelper.renderGuiItemIcon((int)curItemPosX, (int)curItemPosY, itemRenderer, mc);
	    GL11.glPopMatrix();
	    RenderHelper.disableStandardItemLighting();
	    GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		//---------------------------------------------------------------------------------
	    
	    
		//------------------------------Project Info---------------------------------------
	    GL11.glEnable(GL11.GL_BLEND);
	    
		double piPosX = w - w + 4;
		double piPosY = h - h + 4;
		GL11.glPushMatrix();
			GL11.glTranslatef(1f, 1f, 2f);
			this.drawString(mc.fontRenderer, EnumChatFormatting.DARK_AQUA + Constants.NAME + " v." + Constants.VERSION, (int)piPosX, (int)piPosY, 0x000000);
			this.drawString(mc.fontRenderer, EnumChatFormatting.DARK_GREEN + "Pre-Alpha footage.", (int)piPosX, (int)piPosY + 9, 0x000000);
		GL11.glPopMatrix();
		GL11.glDisable(GL11.GL_BLEND);
		//---------------------------------------------------------------------------------
		
		
		//------------------------------Resist Info----------------------------------------
	    GL11.glEnable(GL11.GL_BLEND);
	    
		double resistPosX = w - 19;
		double resistPosY = h - 73;
		String s2 = String.valueOf((int)smp.getDefenseInfo(1, 4));
		String s3 = String.valueOf((int)smp.getDefenseInfo(1, 2));
		String s4 = String.valueOf((int)smp.getDefenseInfo(1, 6));
		String s5 = String.valueOf((int)smp.getDefenseInfo(1, 5));
		
		GL11.glPushMatrix();
			GL11.glTranslatef(1f, 1f, 2f);
			this.drawString(mc.fontRenderer, EnumChatFormatting.GRAY + s2 + "%", (int)resistPosX - mc.fontRenderer.getStringWidth(s2), (int)resistPosY, 0x000000);
			this.drawString(mc.fontRenderer, EnumChatFormatting.GRAY + s3 + "%", (int)resistPosX - mc.fontRenderer.getStringWidth(s3), (int)resistPosY + 11, 0x000000);
			this.drawString(mc.fontRenderer, EnumChatFormatting.GRAY + s4 + "%", (int)resistPosX - mc.fontRenderer.getStringWidth(s4), (int)resistPosY + 23, 0x000000);
			this.drawString(mc.fontRenderer, EnumChatFormatting.GRAY + s5 + "%", (int)resistPosX - mc.fontRenderer.getStringWidth(s5), (int)resistPosY + 35, 0x000000);
		GL11.glPopMatrix();
		GL11.glDisable(GL11.GL_BLEND);
		//---------------------------------------------------------------------------------
	}
	}
}
