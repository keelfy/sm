package net.sm.client.render;

import java.util.Random;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.item.ItemStack;

/**
 * Created by keelfy on 23.07.2016.
 */
public class SMRenderHelper {
    public static void drawTextureCustomSize(double posX, double posY, double posZ, double startPixX, double startPixY, double pieceSizeX, double pieceSizeY, float sizeTextureX, float sizeTextureY)
    {
        float f4 = 1.0F / sizeTextureX;
        float f5 = 1.0F / sizeTextureY;
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawingQuads();
        tessellator.addVertexWithUV((double)posX, (double)(posY + pieceSizeY), posZ, (double)(startPixX * f4), (double)((startPixY + (float)pieceSizeY) * f5));
        tessellator.addVertexWithUV((double)(posX + pieceSizeX), (double)(posY + pieceSizeY), posZ, (double)((startPixX + (float)pieceSizeX) * f4), (double)((startPixY + (float)pieceSizeY) * f5));
        tessellator.addVertexWithUV((double)(posX + pieceSizeX), (double)posY, posZ, (double)((startPixX + (float)pieceSizeX) * f4), (double)(startPixY * f5));
        tessellator.addVertexWithUV((double)posX, (double)posY, posZ, (double)(startPixX * f4), (double)(startPixY * f5));
        tessellator.draw();
    }
    
    public static void renderGuiItemIcon(int x, int y, RenderItem itemRenderer, Minecraft mc) {
        ItemStack itemstack = mc.thePlayer.inventory.getCurrentItem();
        ScaledResolution sr = new ScaledResolution(mc.gameSettings, mc.displayWidth, mc.displayHeight);
        Random rand = new Random();
        int w = sr.getScaledWidth();
        int h = sr.getScaledHeight();
        
        if (itemstack != null)
        {
            /*float ticks = (float)itemstack.animationsToGo - partialTicks;

            if (ticks > 0.0F)
            {
                GL11.glPushMatrix();
                //float f2 = 1.0F + ticks / 5.0F;
                //GL11.glTranslatef((float)(x + 8), (float)(y + 12), 0.0F);
                //GL11.glScalef(1.0F / f2, (f2 + 1.0F) / 2.0F, 1.0F);
                //GL11.glTranslatef((float)(-(x + 8)), (float)(-(y + 12)), 0.0F);
            }*/
        	GL11.glEnable(GL11.GL_BLEND);
            itemRenderer.renderItemIntoGUI(mc.fontRenderer, mc.getTextureManager(), itemstack, x, y);
            GL11.glDisable(GL11.GL_BLEND);

           /* if (ticks > 0.0F)
            {
                GL11.glPopMatrix();
            }*/

           /* if (itemstack.isItemDamaged())
            {
                int k = (int)Math.round(13.0D - (double)itemstack.getItemDamageForDisplay() * 13.0D / (double)itemstack.getMaxDamage());
                int l = (int)Math.round(255.0D - (double)itemstack.getItemDamageForDisplay() * 255.0D / (double)itemstack.getMaxDamage());
                GL11.glDisable(GL11.GL_LIGHTING);
                GL11.glDisable(GL11.GL_DEPTH_TEST);
                GL11.glDisable(GL11.GL_TEXTURE_2D);
                Tessellator tessellator = Tessellator.instance;
                int i1 = 255 - l << 16 | l << 8;
                int j1 = (255 - l) / 4 << 16 | 16128;
                renderQuad(tessellator, x + 2, y + 13, 13, 2, 0);
                renderQuad(tessellator, x + 2, y + 13, 12, 1, j1);
                renderQuad(tessellator, x + 2, y + 13, k, 1, i1);
                GL11.glEnable(GL11.GL_TEXTURE_2D);
                GL11.glEnable(GL11.GL_LIGHTING);
                GL11.glEnable(GL11.GL_DEPTH_TEST);
                GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            }*/
        }
    }
    
    private static void renderQuad(Tessellator par1Tessellator, int x, int y, int w, int h, int color)
    {
        par1Tessellator.startDrawingQuads();
        par1Tessellator.setColorOpaque_I(color);
        par1Tessellator.addVertex((double)(x + 0), (double)(y + 0), 0.0D);
        par1Tessellator.addVertex((double)(x + 0), (double)(y + h), 0.0D);
        par1Tessellator.addVertex((double)(x + w), (double)(y + h), 0.0D);
        par1Tessellator.addVertex((double)(x + w), (double)(y + 0), 0.0D);
        par1Tessellator.draw();
    }
}
