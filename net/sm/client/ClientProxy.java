package net.sm.client;

import cpw.mods.fml.common.FMLCommonHandler;
import net.minecraft.client.Minecraft;
import net.minecraftforge.common.MinecraftForge;
import net.sm.client.gui.GuiPlayerStats;
import net.sm.client.sound.SoundManager;
import net.sm.server.CommonProxy;


public class ClientProxy extends CommonProxy {
	
	
	  public void initMod() {
		  
		  MinecraftForge.EVENT_BUS.register(new SoundManager());
		  //MinecraftForge.EVENT_BUS.register(new overlay(Minecraft.getMinecraft()));
	  }
	  
	  public void registerGui() {
	    	if (FMLCommonHandler.instance().getEffectiveSide().isClient()) {
	    		 MinecraftForge.EVENT_BUS.register(new GuiPlayerStats(Minecraft.getMinecraft()));
	    	}
	    	
	  }
}
