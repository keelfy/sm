package net.sm.client.sound;

import net.minecraftforge.client.event.sound.SoundLoadEvent;
import net.minecraftforge.event.ForgeSubscribe;
import net.sm.Constants;

/**
 * Created by keelfy on 20.07.2016.
 */
public class SoundManager {
	
	private String resDir = Constants.MODID;
	
	@ForgeSubscribe 
	public void onSoundLoading(SoundLoadEvent e) {
	 e.manager.addSound(resDir + ":softdrink.ogg");
	 e.manager.addSound(resDir + ":bandage.ogg");
	 e.manager.addSound(resDir + ":food.ogg");
	 e.manager.addSound(resDir + ":medkit.ogg");
	 e.manager.addSound(resDir + ":alchdrink.ogg");
     e.manager.addSound(resDir + ":pills.ogg");
     e.manager.addSound(resDir + ":psy.ogg");
     e.manager.addSound(resDir + ":heart.ogg");
     e.manager.addSound(resDir + ":kiselh.ogg");
     e.manager.addSound(resDir + ":kisel.ogg");

 }
}
