package net.sm;

import net.minecraft.util.ResourceLocation;

/**
 * Created by keelfy on 22.07.2016.
 */
public class Constants {
	
	//Main
    public static final String MODID = "stalmine";
    public static final String NAME = "S.T.A.L.M.I.N.E: Experimental";
    public static final String VERSION = "0.35";
    public static final String[] AUTHORS = new String[] {"keelfy", "mercury"};
    public static final String CLIENT = "net.sm.client.ClientProxy";
    public static final String SERVER = "net.sm.server.CommonProxy";
    public static final String CHANNEL1 = "smp";
    
    //Resources
    public static final ResourceLocation  guiPlayerStats = new ResourceLocation(MODID, "textures/gui/playerStats.png");
}
