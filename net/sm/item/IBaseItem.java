package net.sm.item;

import net.minecraft.item.Item;

/**
 * Created by keelfy on 18.07.2016.
 */
public interface  IBaseItem  {
	
	public Item setUnlocalizedName(String str);
	
	public float getWeight(Item item);
}
