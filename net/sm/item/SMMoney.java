package net.sm.item;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.common.EnumHelper;
import net.sm.properties.SMPlayer;
import net.sm.registry.module.TabModule;

/**
 * Created by keelfy on 18.07.2016.
 */
public class SMMoney extends Item implements IBaseItem {

	private float weight = 0;
	private int price = 0;
	
    public SMMoney(int id, float weight, int price) {
        super(id);
        this.weight = weight;
        this.price = price;
        setCreativeTab(TabModule.tabMoney);
    }

    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
    	super.onItemRightClick(par1ItemStack, par2World, par3EntityPlayer);

    	//				TO-DO
    	//SMPluginMoneySystem money = new SMMoneyPluginSystem();
    	//money.giveMoney(price);
        return par1ItemStack;
    }
    
    @Override
    public Item setUnlocalizedName(String par1Str)
    {
        super.setUnlocalizedName(par1Str);
        return this;
    }

	@Override
	public float getWeight(Item item) {
		item = this;
		return weight;
	}
}
