package net.sm.item;

import net.minecraft.item.Item;

/**
 * Created by keelfy on 20.07.2016.
 */
public class SMQuestItem extends Item implements IBaseItem {

	private float weight = 0;
	
	public SMQuestItem(int id, float weight) {
		super(id);
		this.weight = weight;
	}

	@Override
	public Item setUnlocalizedName(String par1Str) {
		super.setUnlocalizedName(par1Str);
		return this;
	}
	
	@Override
	public float getWeight(Item item) {
		item = this;
		return weight;
	}

}
