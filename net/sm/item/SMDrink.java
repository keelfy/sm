package net.sm.item;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import net.sm.properties.SMPlayer;
import net.sm.registry.module.TabModule;

/**
 * Created by keelfy on 23.07.2016.
 */
public class SMDrink extends Item implements IBaseItem {

	private float weight = 0;
	private float getDrunkAmount = 0;
	
	public SMDrink(int id, float weight, float getDrunkAmount) {
		super(id);
		this.setMaxStackSize(3);
		this.weight = weight;
		this.getDrunkAmount = getDrunkAmount;
		this.setCreativeTab(TabModule.tabFood);
	}
	
    public ItemStack onEaten(ItemStack is, World world, EntityPlayer player)
    {
        --is.stackSize;
        //par2World.playSoundAtEntity(par3EntityPlayer, "random.burp", 0.5F, par2World.rand.nextFloat() * 0.1F + 0.9F);
        if(!world.isRemote && is.getUnlocalizedName().contains("vodka")) {
        	player.addPotionEffect(new PotionEffect(9, 1200, 0));
        	SMPlayer.get(player).changeAdversity(2, 3, 60);
        	 SMPlayer.get(player).changeAdversity(2, 2, 19);
        } else if(!world.isRemote && is.getUnlocalizedName().contains("konyak")) {
        	player.addPotionEffect(new PotionEffect(9, 1200, 0));
        	SMPlayer.get(player).changeAdversity(2, 3, 30);
        	SMPlayer.get(player).changeAdversity(2, 2, 11);
        	
        } else if(!world.isRemote && is.getUnlocalizedName().contains("redbull")) {
        	player.addPotionEffect(new PotionEffect(8, 1000, 0));
        	player.addPotionEffect(new PotionEffect(1, 1000, 0));
        	SMPlayer.get(player).changeAdversity(1, 2, getDrunkAmount);
      
        } else {
        	SMPlayer.get(player).changeAdversity(1, 2, getDrunkAmount);
        }
        return is;
    }
    
    @Override
    public int getMaxItemUseDuration(ItemStack par1ItemStack) {
        return 32;
    }

    @Override
    public EnumAction getItemUseAction(ItemStack par1ItemStack) {
        return EnumAction.drink;
    }
	
	public ItemStack onItemRightClick(ItemStack is, World world, EntityPlayer player) {
		super.onItemRightClick(is, world, player);

		if(SMPlayer.get(player).getAdversityInfo(1, 2) != SMPlayer.get(player).getAdversityInfo(2, 2)) 
			player.setItemInUse(is, this.getMaxItemUseDuration(is));
		else if(is.getUnlocalizedName().contains("vodka") || is.getUnlocalizedName().contains("konyak"))
			player.setItemInUse(is, this.getMaxItemUseDuration(is));
		return is;
	}

	@Override
	public float getWeight(Item item) {
		item = this;
		return weight;
	}
	
	public float getGetDrunkAmount() {
		return getDrunkAmount;
	}
}
