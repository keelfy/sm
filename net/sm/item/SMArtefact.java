package net.sm.item;

import net.minecraft.item.Item;
import net.sm.registry.module.TabModule;

/**
 * Created by keelfy on 21.07.2016.
 */
public class SMArtefact extends Item implements IBaseItem {

	private float weight = 0;
	
	public SMArtefact(int id, float weight) {
		super(id);
		this.setMaxStackSize(1);
		this.setCreativeTab(TabModule.tabArtefact);
		this.weight = weight;
	}

	@Override
	public Item setUnlocalizedName(String par1Str) {
		super.setUnlocalizedName(par1Str);
		return this;
	}
	
	@Override
	public float getWeight(Item item) {
		item = this;
		return 0;
	}

}
