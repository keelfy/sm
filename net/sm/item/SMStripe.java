package net.sm.item;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.sm.properties.SMPlayer;
import net.sm.registry.module.TabModule;

/**
 * Created by keelfy on 19.07.2016.
 */
public class SMStripe extends Item implements IBaseItem {

	private float weight = 0;
	
	public SMStripe(int id, float weight) {
		super(id);
		this.weight = weight;
		setCreativeTab(TabModule.tabStripe);
	}
	
	@Override
	public float getWeight(Item item) {
		item = this;
		return weight;
	}
}
