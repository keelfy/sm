package net.sm.item;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import net.sm.properties.SMPlayer;

/**
 * Created by keelfy on 19.07.2016.
 */
public class SMMedicine extends Item implements IBaseItem {

	private float weight = 0;
	
	public SMMedicine(int par1, float weight) {
		super(par1);
		this.weight = weight;
	}
	    
	@Override
	public float getWeight(Item item) {
		item = this;
		return weight;
	}

}
