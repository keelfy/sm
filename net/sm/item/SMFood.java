package net.sm.item;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.sm.properties.SMPlayer;
import net.sm.registry.module.TabModule;

/**
 * Created by keelfy on 18.07.2016.
 */
public class SMFood extends ItemFood implements IBaseItem {
	
	private float weight = 0;
	
    public SMFood(int id, float weight, int healAmount) {
        super(id, healAmount, 0.4F, false);
        this.setMaxStackSize(3);
        this.weight = weight;
        setCreativeTab(TabModule.tabFood);
    }
    
    @Override
    public Item setUnlocalizedName(String par1Str)
    {
        super.setUnlocalizedName(par1Str);
        return this;
    }

	@Override
	public float getWeight(Item item) {
		item = this;
		return weight;
	}
}
