package net.sm.registry;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;

/**
 * Created by keelfy on 19.07.2016.
 */
public class BlockRegister<T extends Block>
{
    private static String resourceDomain = "";

    public static void setResourceDomain(String resourceDomain)
    {
        BlockRegister.resourceDomain = resourceDomain;
    }

    public static <T extends Block> BlockRegister<T> of(String name, T block)
    {
        return new BlockRegister<T>(name, block);
    }

    T block;
    String uniqueName;
    String resourceName;

    private BlockRegister(String name, T block)
    {
        this.block = block;
        this.uniqueName = name;
        this.resourceName = name;
    }

    public BlockRegister<T> withResource(String name)
    {
        this.resourceName = name;
        return this;
    }

    public T register()
    {
        block.setUnlocalizedName(resourceDomain + resourceName);
        block.setTextureName(resourceDomain + ":" + resourceName);
        GameRegistry.registerBlock(block, uniqueName);
        return block;
    }
}
