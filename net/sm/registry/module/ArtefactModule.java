package net.sm.registry.module;

import java.util.Random;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import net.sm.StalMine;
import net.sm.item.SMArtefact;
import net.sm.properties.SMPlayer;
import net.sm.registry.ItemRegister;

/**
 * Created by keelfy on 21.07.2016.
 */
public class ArtefactModule {
	
    public static Item[] artefact = new Item[34];
	
	public static void init() {
		artefact[0] = ItemRegister.of("artefact/batareika", new SMArtefact(2500, 0.1f) {
    	    public void onUpdate(ItemStack par1ItemStack, World par2World, Entity par3Entity, int par4, boolean par5) {
    	        super.onUpdate(par1ItemStack, par2World, par3Entity, par4, par5);
    	        EntityPlayer player = (EntityPlayer)par3Entity;
    	        SMPlayer smp = SMPlayer.get(player);
    	        Random rand = new Random();
    	    
    	        if(par3Entity instanceof EntityPlayer) {
    	        	player.addPotionEffect(new PotionEffect(1, 20, 0));
    	        	
    	        	if(rand.nextInt(10) == 0)
    	        		smp.changeAdversity(1, 3, 1);
    	        }
    	        
    	     }	
    	}).register().setUnlocalizedName("batareika");
		
		artefact[1] = ItemRegister.of("artefact/bengal_fire", new SMArtefact(2501, 0.1f) {
    	    public void onUpdate(ItemStack par1ItemStack, World par2World, Entity par3Entity, int par4, boolean par5) {
    	        super.onUpdate(par1ItemStack, par2World, par3Entity, par4, par5);
    	        EntityPlayer player = (EntityPlayer)par3Entity;
    	        SMPlayer smp = SMPlayer.get(player);
    	        Random rand = new Random();
    	        
    	        if(par3Entity instanceof EntityPlayer) {
    	        	smp.changeDefense(1, 3, 25);
    	        	
    	        	if(rand.nextInt(60) == 1) 
    	        		smp.changeAdversity(1, 3, 1);
    	        	
    	        }
    	        
    	     }	
    	}).register().setUnlocalizedName("bengal_fire");
	}
}
