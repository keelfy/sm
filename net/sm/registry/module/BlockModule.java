package net.sm.registry.module;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.sm.block.BlockWire;
import net.sm.block.SMDeco;
import net.sm.block.anomaly.AnomalyElectra;
import net.sm.registry.BlockRegister;

/**
 * Created by keelfy on 19.07.2016.
 */
public class BlockModule {
	
	public static Block[] deco =  new Block[10];
	public static Block[] envirement = new Block[10]; 
	public static Block[] anomaly = new Block[10];
	
	public static void init() {
		 initDeco();
		 initEnvirement();
		 initAnomaly();
	}
	
	public static void initDeco() {
		deco[0] = BlockRegister.of("deco/hromakey", new SMDeco(3000, Material.cloth)).register().setUnlocalizedName("hromakey");
	}
	
	public static void initEnvirement() {
		envirement[0] = BlockRegister.of("envirement/wire", new BlockWire(3001)).register().setUnlocalizedName("wire");
		
	}
	
	public static void initAnomaly() {
		anomaly[0] = BlockRegister.of("anomaly/electra", new AnomalyElectra(3006, 10)).register().setUnlocalizedName("electra");
	}
}
