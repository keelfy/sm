package net.sm.registry.module;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.potion.PotionEffect;
import net.sm.registry.module.ItemModule;

/**
 * Created by keelfy on 18.07.2016.
 */
public class TabModule {

    public static final CreativeTabs tabFood = new CreativeTabs("tabFood") {
        @Override
        public Item getTabIconItem() {
            return ItemModule.food[33];
        }
    };

    public static final CreativeTabs tabMoney = new CreativeTabs("tabMoney") {
        @Override
        public Item getTabIconItem() {
            return ItemModule.money[5];
        }
    };
    
    public static final CreativeTabs tabStripe = new CreativeTabs("tabStripe") {
        @Override
        public Item getTabIconItem() {
            return ItemModule.stripe[0];
        }
    };
    
    public static final CreativeTabs tabDeco = new CreativeTabs("tabDeco") {
        @Override
        public int getTabIconItemIndex() {
            return BlockModule.deco[0].blockID;
        }
    };
    
    public static final CreativeTabs tabEnvirement = new CreativeTabs("tabEnvirement") {
        @Override
        public int getTabIconItemIndex() {
            return BlockModule.envirement[0].blockID;
        }
    };
    
    public static final CreativeTabs tabArtefact = new CreativeTabs("tabArtefact") {
        @Override
        public Item getTabIconItem() {
            return ArtefactModule.artefact[0];
        }
    };
    
    public static final CreativeTabs tabAnomaly = new CreativeTabs("tabAnomaly") {
        @Override
        public int getTabIconItemIndex() {
            return BlockModule.anomaly[0].blockID;
        }
    };
}
