package net.sm.registry.module;

import net.minecraftforge.common.MinecraftForge;
import net.sm.registry.event.SMEventHandler;

/**
 * Created by keelfy on 20.07.2016.
 */
public class EventModule {
	public static void init() {
		MinecraftForge.EVENT_BUS.register(new SMEventHandler());
	}
}
