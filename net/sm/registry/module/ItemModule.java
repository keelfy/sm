package net.sm.registry.module;

import java.util.Random;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import net.sm.StalMine;
import net.sm.item.SMArtefact;
import net.sm.item.SMDrink;
import net.sm.item.SMFood;
import net.sm.item.SMMoney;
import net.sm.item.SMQuestItem;
import net.sm.item.SMStripe;
import net.sm.properties.SMPlayer;
import net.sm.registry.ItemRegister;

/**
 * Created by keelfy on 18.07.2016.
 */
public class ItemModule {

    public static Item[] money = new Item[10];
    public static Item[] food = new Item[50];
    public static Item[] stripe = new Item[7];
    public static Item[] questItems = new Item[1];
    
    public static void init() {
        initFood();
        initMoney();
        initStripe();
        initQuestItems();
        ArtefactModule.init();
        MedicineModule.init();
        
    }


	public static void initMoney() {
        money[0] = ItemRegister.of("money/rub1", new SMMoney(2050, 0.0f, 1)).register().setUnlocalizedName("rub1");
        money[1] = ItemRegister.of("money/rub2", new SMMoney(2051, 0.0f, 2)).register().setUnlocalizedName("rub2");
        money[2] = ItemRegister.of("money/rub5", new SMMoney(2052, 0.0f, 5)).register().setUnlocalizedName("rub5");
        money[3] = ItemRegister.of("money/rub10", new SMMoney(2053, 0.0f, 10)).register().setUnlocalizedName("rub10");
        money[4] = ItemRegister.of("money/rub50", new SMMoney(2054, 0.0f, 50)).register().setUnlocalizedName("rub50");
        money[5] = ItemRegister.of("money/rub100", new SMMoney(2055, 0.0f, 100)).register().setUnlocalizedName("rub100");
        money[6] = ItemRegister.of("money/rub100s", new SMMoney(2056, 0.0f, 100)).register().setUnlocalizedName("rub100s");
        money[7] = ItemRegister.of("money/rub500", new SMMoney(2057, 0.0f, 500)).register().setUnlocalizedName("rub500");
        money[8] = ItemRegister.of("money/rub1000", new SMMoney(2058, 0.0f, 1000)).register().setUnlocalizedName("rub1000");
        money[9] = ItemRegister.of("money/rub5000", new SMMoney(2059, 0.0f, 5000)).register().setUnlocalizedName("rub5000");
    }

    public static void initFood() {
        food[0] = ItemRegister.of("food/alpen1", new SMFood(2000, 0.1f,  2)).register().setUnlocalizedName("alpen1");
        food[1] = ItemRegister.of("food/alpen2", new SMFood(2001, 0.1f,  2)).register().setUnlocalizedName("alpen2");
        food[2] = ItemRegister.of("food/alpen3", new SMFood(2002, 0.1f,  2)).register().setUnlocalizedName("alpen3");
        food[3] = ItemRegister.of("food/ananas", new SMFood(2003, 0.5f,  3)).register().setUnlocalizedName("ananas");
        food[4] = ItemRegister.of("food/assorti", new SMFood(2004, 0.2f,  8)).register().setUnlocalizedName("assorti");
        food[5] = ItemRegister.of("food/banana", new SMFood(2005, 0.1f,  3)).register().setUnlocalizedName("banana");
        food[6] = ItemRegister.of("food/bonduelle", new SMFood(2006, 0.4f,  7)).register().setUnlocalizedName("bonduelle");
        food[7] = ItemRegister.of("food/bonpari1", new SMFood(2007, 0.1f,  3)).register().setUnlocalizedName("bonpari1");
        food[8] = ItemRegister.of("food/bonpari2", new SMFood(2008, 0.1f,  3)).register().setUnlocalizedName("bonpari2");
        food[9] = ItemRegister.of("food/bonpari3", new SMFood(2009, 0.1f,  3)).register().setUnlocalizedName("bonpari3");
        food[10] = ItemRegister.of("food/bonpari4", new SMFood(2010, 0.1f,  3)).register().setUnlocalizedName("bonpari4");
        food[11] = ItemRegister.of("food/file1", new SMFood(2011, 0.1f,  10)).register().setUnlocalizedName("file1");
        food[12] = ItemRegister.of("food/file2", new SMFood(2012, 0.1f,  10)).register().setUnlocalizedName("file2");
        food[13] = ItemRegister.of("food/file3", new SMFood(2013, 0.1f,  10)).register().setUnlocalizedName("file3");
        food[14] = ItemRegister.of("food/file4", new SMFood(2014, 0.1f,  10)).register().setUnlocalizedName("file4");
        food[15] = ItemRegister.of("food/file5", new SMFood(2015, 0.1f,  10)).register().setUnlocalizedName("file5");
        food[16] = ItemRegister.of("food/file6", new SMFood(2016, 0.1f,  10)).register().setUnlocalizedName("file6");
        food[17] = ItemRegister.of("food/flyaga", new SMDrink(2017, 0.1f,  30)).register().setUnlocalizedName("flyaga");
        food[18] = ItemRegister.of("food/govyadina", new SMFood(2018, 0.1f,  6)).register().setUnlocalizedName("govyadina");
        food[19] = ItemRegister.of("food/hienz", new SMFood(2019, 0.1f,  6)).register().setUnlocalizedName("hienz");
        food[20] = ItemRegister.of("food/hleb1", new SMFood(2020, 0.1f,  2)).register().setUnlocalizedName("hleb1");
        food[21] = ItemRegister.of("food/hleb2", new SMFood(2021, 0.1f,  4)).register().setUnlocalizedName("hleb2");
        food[22] = ItemRegister.of("food/kilka", new SMFood(2022, 0.1f,  7)).register().setUnlocalizedName("kilka");
        food[23] = ItemRegister.of("food/kolbosa1", new SMFood(2023, 0.1f,  7)).register().setUnlocalizedName("kolbosa1");
        food[24] = ItemRegister.of("food/kolbosa2", new SMFood(2024, 0.1f,  7)).register().setUnlocalizedName("kolbosa2");
        food[25] = ItemRegister.of("food/konyak", new SMDrink(2025, 0.1f,  -20)).register().setUnlocalizedName("konyak");
        food[26] = ItemRegister.of("food/mandarin", new SMFood(2026, 0.1f,  2)).register().setUnlocalizedName("mandarin");
        food[27] = ItemRegister.of("food/mre", new SMFood(2027, 0.1f,  20)).register().setUnlocalizedName("mre");
        food[28] = ItemRegister.of("food/myaso", new SMFood(2028, 0.1f,  6)).register().setUnlocalizedName("myaso");
        food[29] = ItemRegister.of("food/ogyrci", new SMFood(2029, 0.1f,  6)).register().setUnlocalizedName("ogyrci");
        food[30] = ItemRegister.of("food/oreo", new SMFood(2030, 0.1f,  3)).register().setUnlocalizedName("oreo");
        food[31] = ItemRegister.of("food/oreobig", new SMFood(2031, 0.1f,  12)).register().setUnlocalizedName("oreobig");
        food[32] = ItemRegister.of("food/pechenie1", new SMFood(2032, 0.1f,  4)).register().setUnlocalizedName("pechenie1");
        food[33] = ItemRegister.of("food/pizza", new SMFood(2033, 0.1f,  15)).register().setUnlocalizedName("pizza");
        food[34] = ItemRegister.of("food/redbull", new SMDrink(2034, 0.1f,  20)).register().setUnlocalizedName("redbull");
        food[35] = ItemRegister.of("food/rolton1", new SMFood(2035, 0.1f,  5)).register().setUnlocalizedName("rolton1");
        food[36] = ItemRegister.of("food/rolton2", new SMFood(2036, 0.1f,  5)).register().setUnlocalizedName("rolton2");
        food[37] = ItemRegister.of("food/saira", new SMFood(2037, 0.1f,  8)).register().setUnlocalizedName("saira");
        food[38] = ItemRegister.of("food/sendwich", new SMFood(2038, 0.1f,  12)).register().setUnlocalizedName("sendwich");
        food[39] = ItemRegister.of("food/shproti", new SMFood(2039, 0.1f,  8)).register().setUnlocalizedName("shproti");
        food[40] = ItemRegister.of("food/sok", new SMDrink(2040, 0.1f,  15)).register().setUnlocalizedName("sok");
        food[41] = ItemRegister.of("food/spam", new SMFood(2041, 0.1f,  10)).register().setUnlocalizedName("spam");
        food[42] = ItemRegister.of("food/sufle1", new SMFood(2042, 0.1f,  6)).register().setUnlocalizedName("sufle1");
        food[43] = ItemRegister.of("food/sufle2", new SMFood(2043, 0.1f,  6)).register().setUnlocalizedName("sufle2");
        food[44] = ItemRegister.of("food/sufle3", new SMFood(2044, 0.1f,  6)).register().setUnlocalizedName("sufle3");
        food[45] = ItemRegister.of("food/sufle4", new SMFood(2045, 0.1f,  6)).register().setUnlocalizedName("sufle4");
        food[46] = ItemRegister.of("food/tynec", new SMFood(2046, 0.1f,  8)).register().setUnlocalizedName("tynec");
        food[47] = ItemRegister.of("food/voda", new SMDrink(2047, 0.1f,  45)).register().setUnlocalizedName("voda");
        food[48] = ItemRegister.of("food/vodka", new SMDrink(2048, 0.1f,  -40)).register().setUnlocalizedName("vodka");
        food[49] = ItemRegister.of("food/yabloko", new SMFood(2049, 0.1f,  2)).register().setUnlocalizedName("yabloko");
    }
    
    public static void initStripe() {
    	stripe[0] = ItemRegister.of("stripe/bandit", new SMStripe(2060, 0.0f)).register().setUnlocalizedName("bandit");
    	stripe[1] = ItemRegister.of("stripe/dolg", new SMStripe(2061, 0.0f)).register().setUnlocalizedName("dolg");
    	stripe[2] = ItemRegister.of("stripe/military", new SMStripe(2062, 0.0f)).register().setUnlocalizedName("military");
    	stripe[3] = ItemRegister.of("stripe/monolith", new SMStripe(2063, 0.0f)).register().setUnlocalizedName("monolith");
    	stripe[4] = ItemRegister.of("stripe/naemnik", new SMStripe(2064, 0.0f)).register().setUnlocalizedName("naemnik");
    	stripe[5] = ItemRegister.of("stripe/stalker", new SMStripe(2065, 0.0f)).register().setUnlocalizedName("stalker");
    	stripe[6] = ItemRegister.of("stripe/svoboda", new SMStripe(2066, 0.0f)).register().setUnlocalizedName("svoboda");
    }
    
    public static void initQuestItems() {
    	questItems[0] = ItemRegister.of("questItem/comp", new SMQuestItem(2067, 2.5f)).register().setUnlocalizedName("comp");
    }
}
