package net.sm.registry.event;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.EventPriority;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.EntityEvent.EntityConstructing;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.sm.properties.SMPlayer;

/**
 * Created by keelfy on 20.07.2016.
 */
public class SMEventHandler {
	
	@ForgeSubscribe(priority=EventPriority.HIGHEST)
	public void onEntityConstructing(EntityConstructing event) {
		
		if (event.entity instanceof EntityPlayer && SMPlayer.get((EntityPlayer) event.entity) == null)
			SMPlayer.register((EntityPlayer) event.entity);
		
	}
	
	@ForgeSubscribe(priority=EventPriority.HIGHEST)
	public void onEntityJoinWorld(EntityJoinWorldEvent event)
	{
		if (!event.entity.worldObj.isRemote && event.entity instanceof EntityPlayer)
			SMPlayer.get((EntityPlayer) event.entity).sync();
		
		if(event.entity instanceof EntityPlayer) {
			EntityPlayer player = (EntityPlayer) event.entity;

            player.getEntityAttribute(SharedMonsterAttributes.maxHealth).setAttribute(100.0D);
            player.heal(100.0f);
		}
	}
	
	@ForgeSubscribe
	public void breakSpeed(PlayerEvent.BreakSpeed event) {
		if(!event.entityPlayer.capabilities.isCreativeMode)
			event.newSpeed = 0;
	}
}
