package net.sm.properties.weight;

import net.minecraft.item.Item;

/**
 * Created by keelfy on 21.07.2016.
 */
public class VanillaItem {
	public static float getVanilaItem(Item item, int metadata) {
		if (item == Item.shovelIron) return IVanilaItem.iron_shovel;
		else if (item == Item.pickaxeIron) return IVanilaItem.iron_pickaxe;
		else if (item == Item.axeIron) return IVanilaItem.iron_axe;
		else if (item == Item.flintAndSteel) return IVanilaItem.flint_and_steel;
		else if (item == Item.appleRed) return IVanilaItem.apple;
		else if (item == Item.bow) return IVanilaItem.bow;
		else if (item == Item.arrow) return IVanilaItem.arrow;
		else if (item == Item.coal && metadata == 0) return IVanilaItem.coal;
		else if (item == Item.coal && metadata == 1) return IVanilaItem.charcoal;
		else if (item == Item.diamond) return IVanilaItem.diamond;
		else if (item == Item.ingotIron) return IVanilaItem.iron_ingot;
		else if (item == Item.ingotGold) return IVanilaItem.gold_ingot;
		else if (item == Item.swordIron) return IVanilaItem.iron_sword;
		else if (item == Item.swordWood) return IVanilaItem.wooden_sword;
		else if (item == Item.shovelWood) return IVanilaItem.wooden_shovel;
		else if (item == Item.pickaxeWood) return IVanilaItem.wooden_pickaxe;
		else if (item == Item.axeWood) return IVanilaItem.wooden_axe;
		else if (item == Item.swordStone) return IVanilaItem.stone_sword;
		else if (item == Item.shovelStone) return IVanilaItem.stone_shovel;
		else if (item == Item.pickaxeStone) return IVanilaItem.stone_pickaxe;
		else if (item == Item.axeStone) return IVanilaItem.stone_axe;
		else if (item == Item.swordDiamond) return IVanilaItem.diamond_sword;
		else if (item == Item.shovelDiamond) return IVanilaItem.diamond_shovel;
		else if (item == Item.pickaxeDiamond) return IVanilaItem.diamond_pickaxe;
		else if (item == Item.axeDiamond) return IVanilaItem.diamond_axe;
		else if (item == Item.stick) return IVanilaItem.stick;
		else if (item == Item.bowlEmpty) return IVanilaItem.bowl;
		else if (item == Item.swordGold) return IVanilaItem.golden_sword;
		else if (item == Item.shovelGold) return IVanilaItem.golden_shovel;
		else if (item == Item.pickaxeGold) return IVanilaItem.golden_pickaxe;
		else if (item == Item.axeGold) return IVanilaItem.golden_axe;
		else if (item == Item.feather) return IVanilaItem.feather;
		else if (item == Item.gunpowder) return IVanilaItem.gunpowder;
		else if (item == Item.hoeWood) return IVanilaItem.wooden_hoe;
		else if (item == Item.hoeStone) return IVanilaItem.stone_hoe;
		else if (item == Item.hoeIron) return IVanilaItem.iron_hoe;
		else if (item == Item.hoeDiamond) return IVanilaItem.diamond_hoe;
		else if (item == Item.hoeGold) return IVanilaItem.golden_hoe;
		else if (item == Item.seeds) return IVanilaItem.wheat_seeds;
		else if (item == Item.wheat) return IVanilaItem.wheat;
		else if (item == Item.bread) return IVanilaItem.bread;
		else if (item == Item.flint) return IVanilaItem.flint;
		else if (item == Item.porkRaw) return IVanilaItem.porkchop;
		else if (item == Item.porkCooked) return IVanilaItem.cooked_porkchop;
		else if (item == Item.painting) return IVanilaItem.painting;
		else if (item == Item.appleGold && metadata == 0) return IVanilaItem.golden_apple_0;
		else if (item == Item.appleGold && metadata == 1) return IVanilaItem.golden_apple_1;
		else if (item == Item.sign) return IVanilaItem.sign;
		else if (item == Item.doorWood) return IVanilaItem.wooden_door;
		else if (item == Item.bucketEmpty) return IVanilaItem.bucket;
		else if (item == Item.bucketWater) return IVanilaItem.water_bucket;
		else if (item == Item.bucketLava) return IVanilaItem.lava_bucket;
		else if (item == Item.minecartEmpty) return IVanilaItem.minecart;
		else if (item == Item.saddle) return IVanilaItem.saddle;
		else if (item == Item.doorIron) return IVanilaItem.iron_door;
		else if (item == Item.redstone) return IVanilaItem.redstone;
		else if (item == Item.snowball) return IVanilaItem.snowball;
		else if (item == Item.boat) return IVanilaItem.boat;
		else if (item == Item.leather) return IVanilaItem.leather;
		else if (item == Item.bucketMilk) return IVanilaItem.milk_bucket;
		else if (item == Item.brick) return IVanilaItem.brick;
		else if (item == Item.clay) return IVanilaItem.clay_ball;
		else if (item == Item.reed) return IVanilaItem.reeds;
		else if (item == Item.paper) return IVanilaItem.paper;
		else if (item == Item.book) return IVanilaItem.book;
		else if (item == Item.slimeBall) return IVanilaItem.slime_ball;
		else if (item == Item.minecartCrate) return IVanilaItem.chest_minecart;
		else if (item == Item.minecartPowered) return IVanilaItem.furnace_minecart;
		else if (item == Item.egg) return IVanilaItem.egg;
		else if (item == Item.compass) return IVanilaItem.compass;
		else if (item == Item.fishingRod) return IVanilaItem.fishing_rod;
		else return 0.0F;
	}
}
