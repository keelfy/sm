package net.sm.properties.weight;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.sm.item.IBaseItem;
import net.sm.properties.SMPlayer;

/**
 * Created by keelfy on 21.07.2016.
 */
public class ItemChecking {
	
	public static float sendInv(EntityPlayer player) {
		float weight = 0.0F;

		for (ItemStack slot : player.inventory.mainInventory)
			weight += checkWeight(slot);

		//for (ItemStack slot : player.inventory.armorInventory)
		//	weight += (checkWeighting(slot) / 2.0F);

		return weight;
	}
	
	public static float checkWeight(ItemStack slot) {
		float weighting = 0.0F;
		if (slot != null) {
			Item item = slot.getItem();
			
			if (item instanceof IBaseItem) 
				weighting += ((IBaseItem)item).getWeight(item) * slot.stackSize;
			else if (VanillaItem.getVanilaItem(item, slot.getItemDamage()) != 0.0F)
				weighting += VanillaItem.getVanilaItem(item, slot.getItemDamage()) * slot.stackSize;

		}
		return weighting;
	}
	
	public static void playerMoving(EntityPlayer player) {
		SMPlayer smp = SMPlayer.get(player);
		
		if (player != null && player.worldObj != null && !player.capabilities.isCreativeMode && smp != null) {

			float curWeight = ItemChecking.sendInv(player);
			float maxWeight = smp.getMaxWeight();
			float overweight = smp.getOverweight();
			
			//System.out.print(curWeight + "/" + maxWeight + "/" + overweight);
			double moving = (double)(((curWeight >= 0.0F && curWeight <= overweight) ? (overweight - curWeight) : 0.0D) / overweight);

			//if(curWeight >= 0.0d && curWeight <= overweight) 
			//	moving = overweight - curWeight;
			//else 
			player.fallDistance += (1.0F - (float)(moving > 0.0D ? (moving * moving) : 0.0D));

			if (player.isAirBorne && moving < 1.0D) {

				if (player.motionY <= 0.0D) {
					player.motionY *= (2.0D - moving);
				} else {
					player.motionY *= (moving > 0.0D ? (moving / moving) : 0.0D);
				}

				if (moving <= 0.0D && player.motionY > 0.0D)
					player.motionY -= player.motionY;

			} else {

				if (player.motionY >= 0.0D)
					player.motionY *= 1.1D;
				else
					player.motionY *= 0.95D;

				if (player.onGround)
					moving *= 1.25D;

				player.motionX *= moving;
				player.motionZ *= moving;

			}

		}
	}
	//--------------------------------------------------------
}
