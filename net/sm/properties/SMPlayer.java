package net.sm.properties;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.common.network.Player;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.world.World;
import net.minecraftforge.common.IExtendedEntityProperties;
import net.sm.Constants;

/**
 * Created by keelfy on 20.07.2016.
 */
public class SMPlayer implements IExtendedEntityProperties {

	public final static String EXT_PROP_NAME = "SMPlayer";
	private EntityPlayer player;
	
	private float currentStamina, maxStamina = 100,
				currentThirst, maxThirst = 100,
				currentRadiation, maxRadiation = 400,
				currentBleeding, maxBleeding = 4,
				currentPsy, maxPsy = 400,
				currentChemical, maxChemical = 400;
	
	private float maxWeight, overweight;
	
	private float maxResist = 100,
				dmgResist, gapResist, electroResist,
				chemResist, radResist, psyResist,
				staminaBoost, burstResist;

	public SMPlayer(EntityPlayer player)
	{
		this.player = player;
		
		//Weight
		maxWeight = 50; overweight = 60;
		
		//Adversity
		currentStamina = maxStamina;
		currentThirst = maxThirst;
		currentRadiation = 0;
		currentBleeding = 0;
		currentPsy = 0;
		currentChemical = 0;
		
		//Defense
		dmgResist = 1;
		gapResist = 1;
		electroResist = 1;
		chemResist = 1;
		radResist = 1;
		psyResist = 1;
		staminaBoost = 1;
		burstResist = 1;
	}
	
	public static final void register(EntityPlayer player)
	{
		player.registerExtendedProperties(SMPlayer.EXT_PROP_NAME, new SMPlayer(player));
	}
	
	public static final SMPlayer get(EntityPlayer player)
	{
		return (SMPlayer) player.getExtendedProperties(EXT_PROP_NAME);
	}
	
	@Override
	public void saveNBTData(NBTTagCompound compound)
	{
		NBTTagCompound properties = new NBTTagCompound();
		
		//Weight
		properties.setFloat("MaxWeight", this.maxWeight);
		properties.setFloat("Overweight", this.overweight);
		
		//Adversity
		properties.setFloat("CurrentStamina", this.currentStamina);
		properties.setFloat("CurrentThirst", this.currentThirst);
		properties.setFloat("CurrentRadiation", this.currentRadiation);
		properties.setFloat("CurrentBleeding", this.currentBleeding);
		properties.setFloat("CurrentPsy", this.currentPsy);	
		properties.setFloat("CurrentChemical", this.currentChemical);	
		
		//Defense
		properties.setFloat("DMGResist", this.dmgResist);
		properties.setFloat("GapResist", this.gapResist);
		properties.setFloat("ElectroResist", this.electroResist);
		properties.setFloat("ChemResist", this.chemResist);
		properties.setFloat("RadResist", this.radResist);
		properties.setFloat("PsyResist", this.psyResist);
		properties.setFloat("StaminaBoost", this.staminaBoost);
		properties.setFloat("BurstResist", this.burstResist);
		
		compound.setTag(EXT_PROP_NAME, properties);
		
	}

	@Override
	public void loadNBTData(NBTTagCompound compound)
	{
		NBTTagCompound properties = (NBTTagCompound) compound.getTag(EXT_PROP_NAME);
		
		//Weight
		this.maxWeight = properties.getFloat("MaxWeight");
		this.overweight = properties.getFloat("Overweight");
		
		//Adversity
		this.currentStamina = properties.getFloat("CurrentStamina");
		this.currentThirst = properties.getFloat("CurrentThirst");
		this.currentRadiation = properties.getFloat("CurrentRadiation");
		this.currentBleeding = properties.getFloat("CurrentBleeding");
		this.currentPsy = properties.getFloat("CurrentPsy");
		this.currentChemical = properties.getFloat("CurrentChemical");
		
		//Defense
		this.dmgResist = properties.getFloat("DMGResist");
		this.gapResist = properties.getFloat("GapResist");
		this.electroResist = properties.getFloat("ElectroResist");
		this.chemResist = properties.getFloat("ChemResist");
		this.radResist = properties.getFloat("RadResist");
		this.psyResist = properties.getFloat("PsyResist");
		this.staminaBoost = properties.getFloat("StaminaResist");
		this.burstResist = properties.getFloat("BurstResist");
		
	}
	
	public final void sync()
	{
		ByteArrayOutputStream bos = new ByteArrayOutputStream(8);
		DataOutputStream outputStream = new DataOutputStream(bos);
		
		try {
			outputStream.writeFloat(this.currentStamina);
			outputStream.writeFloat(this.currentThirst);
			outputStream.writeFloat(this.currentRadiation);
			outputStream.writeFloat(this.currentBleeding);
			outputStream.writeFloat(this.currentPsy);
			outputStream.writeFloat(this.currentChemical);
			outputStream.writeFloat(this.dmgResist);
			outputStream.writeFloat(this.gapResist);
			outputStream.writeFloat(this.chemResist);
			outputStream.writeFloat(this.radResist);
			outputStream.writeFloat(this.psyResist);
			outputStream.writeFloat(this.staminaBoost);
			outputStream.writeFloat(this.maxWeight);
			outputStream.writeFloat(this.overweight);
			outputStream.writeFloat(this.burstResist);
			
		} catch (Exception ex) {
			ex.getStackTrace();
		}

		Packet250CustomPayload packet = new Packet250CustomPayload(Constants.CHANNEL1, bos.toByteArray());
		
		if (FMLCommonHandler.instance().getEffectiveSide().isServer()) {
			EntityPlayerMP player1 = (EntityPlayerMP) player;
			PacketDispatcher.sendPacketToPlayer(packet, (Player) player1);
		}
	}
	
	@Override
	public void init(Entity entity, World world) {}

	public void set(int type, float amount) {
		switch(type) {
		case 0: currentStamina = amount; break;
		case 1: currentThirst = amount; break;
		case 2: currentRadiation = amount; break;
		case 3: currentBleeding = amount; break;
		case 4: currentPsy = amount; break;
		case 5: currentChemical = amount; break;
		case 6: dmgResist = amount; break;
		case 7: gapResist = amount; break;
		case 8: chemResist = amount; break;
		case 9: radResist = amount; break;
		case 10: psyResist = amount; break;
		case 11: staminaBoost = amount; break;
		case 12: maxWeight = amount; break;
		case 13: overweight = amount; break;
		case 14: burstResist = amount; break;
		}
	}
	
	/**
	 * actIndex:
	 * 		1 - increase;      
	 * 		2 - decrease;         
	 * 		3 - reset;            
	 * */
	public void changeAdversity(int actIndex, int adversityIndex, float amount) {
		if(amount > 0) {
			switch(actIndex) {
				case 1: increaseAdversity(adversityIndex, amount); break;
				case 2: decreaseAdversity(adversityIndex, amount); break;
				case 3: resetAdversity(adversityIndex); break;
			}
		}
	}
	
	private void increaseAdversity(int adversityIndex, float amount) {
		switch(adversityIndex) {
			case 1: currentStamina = getAdv(true, amount, adversityIndex); sync(); break;
			case 2: currentThirst = getAdv(true, amount, adversityIndex); sync(); break;
			case 3: currentRadiation = getAdv(true, amount, adversityIndex); sync(); break;
			case 4: currentBleeding = getAdv(true, amount, adversityIndex); sync(); break;
			case 5: currentPsy = getAdv(true, amount, adversityIndex); sync(); break;
			case 6: currentChemical = getAdv(true, amount, adversityIndex); sync(); break;
		}
	}
	
	private void decreaseAdversity(int adversityIndex, float amount) {
		switch(adversityIndex) {
			case 1: currentStamina = getAdv(false, amount, adversityIndex); sync(); break;
			case 2: currentThirst = getAdv(false, amount, adversityIndex); sync(); break;
			case 3: currentRadiation = getAdv(false, amount, adversityIndex); sync(); break;
			case 4: currentBleeding = getAdv(false, amount, adversityIndex); sync(); break;
			case 5: currentPsy = getAdv(false, amount, adversityIndex); sync(); break;
			case 6: currentChemical = getAdv(false, amount, adversityIndex); sync(); break;
		}
	}
	
	public void resetAdversity(int adversityIndex) {
		switch(adversityIndex) {
			case 1: currentStamina = 1;  	sync(); break;
			case 2: currentThirst = 1;		sync(); break;
			case 3: currentRadiation = 1;	sync(); break;
			case 4: currentBleeding = 1;	sync(); break;
			case 5: currentPsy = 1;			sync(); break;
			case 6: currentChemical = 1;	sync(); break;
		}
	}

	/**
	 * actIndex:
	 * 		1 - Get Current;      
	 * 		2 - Get Max;                     
	 * */
	public float getAdversityInfo(int actIndex, int adversityIndex) {
		switch(actIndex) {
			case 1: 
				switch(adversityIndex) {
					case 1: return currentStamina;
					case 2: return currentThirst;
					case 3: return currentRadiation;
					case 4: return currentBleeding;
					case 5: return currentPsy;
					case 6: return currentChemical;
				}
			case 2:
				switch(adversityIndex) {
					case 1: return maxStamina;
					case 2: return maxThirst;
					case 3: return maxRadiation;
					case 4: return maxBleeding;
					case 5: return maxPsy;
					case 6: return maxChemical;
				}
			default:
				System.err.println("Can't get info about player adversitities.");
				return 1;
		}
	}

	/**
	 * actIndex:
	 * 		1 - increase;      
	 * 		2 - decrease;         
	 * 		3 - reset;         
	 * */
	public void changeDefense(int actIndex, int defIndex, float amount) {
		if(amount > 0) {
			switch(actIndex) {
				case 1: increaseDefense(defIndex, amount); break;
				case 2: decreaseDefense(defIndex, amount); break;
				case 3: resetDefense(defIndex); break;
			}
		}
	}
	
	private void increaseDefense(int defIndex, float amount) {
		switch(defIndex) {
			case 1: dmgResist = getDef(true, amount, dmgResist); sync(); break;
			case 2: gapResist = getDef(true, amount, gapResist); sync(); break;
			case 3: electroResist = 	getDef(true, amount, electroResist);; sync(); break;
			case 4: chemResist = getDef(true, amount, chemResist); sync(); break;
			case 5: radResist = getDef(true, amount, radResist); sync(); break;
			case 6: psyResist = getDef(true, amount, psyResist); sync(); break;
			case 7: staminaBoost = getDef(true, amount, staminaBoost); sync(); break;
			case 8: burstResist = getDef(true, amount, burstResist); sync(); break;
		}
	}
	
	private void decreaseDefense(int defIndex, float amount) {
		switch(defIndex) {
			case 1: dmgResist = getDef(false, amount, dmgResist); sync(); break;
			case 2: gapResist = getDef(false, amount, gapResist); sync(); break;
			case 3: electroResist = getDef(false, amount, electroResist); sync(); break;
			case 4: chemResist = getDef(false, amount, chemResist); sync(); break;
			case 5: radResist = getDef(false, amount, radResist); sync(); break;
			case 6: psyResist = getDef(false, amount, psyResist); sync(); break;
			case 7: staminaBoost = getDef(false, amount, staminaBoost); sync(); break;
			case 8: burstResist = getDef(false, amount, burstResist); sync(); break;
		}
	}
	
	public void resetDefense(int defIndex) {
		switch(defIndex) {
			case 1: dmgResist = 1;  	sync(); break;
			case 2: gapResist = 1;		sync(); break;
			case 3: electroResist = 1;	sync(); break;
			case 4: chemResist = 1;		sync(); break;
			case 5: radResist = 1;		sync(); break;
			case 6: psyResist = 1;		sync(); break;
			case 7: staminaBoost = 1;	sync(); break;
			case 8: burstResist = 1;	sync(); break;
		}
	}

	/**
	 * actIndex:
	 * 		1 - Get Current;      
	 * 		2 - Get Max;                     
	 * */
	public float getDefenseInfo(int actIndex, int defIndex) {
		switch(actIndex) {
			case 1: 
				switch(defIndex) {
					case 1: return dmgResist;
					case 2: return gapResist;
					case 3: return electroResist;
					case 4: return chemResist;
					case 5: return radResist;
					case 6: return psyResist;
					case 7: return staminaBoost;
					case 8: return burstResist;
				}
			case 2:
				switch(defIndex) {
					case 1: return maxResist;
					case 2: return maxResist;
					case 3: return maxResist;
					case 4: return maxResist;
					case 5: return maxResist;
					case 6: return maxResist;
					case 7: return maxResist;
				}
			default:
				System.err.println("Can't get info about player defense.");
				return 1;
		}
	}

	
	private float getDef(boolean act, float amount, float current) {
		if(act) 
			return (amount + current < maxResist) ? (current + amount) : (current = maxResist);
		else if(!act) 
			return (current - amount > 1) ? (current - amount) 	: 1;
		return 1;
	}
	
	private float getAdv(boolean act, float amount, int advIndex) {
		if(act) {
			switch(advIndex) {
				case 1:
					float r1 = this.getDefenseInfo(1, 7) * amount / 100;
					return (r1 + currentStamina < maxStamina) ? (currentStamina + r1) : (currentStamina = maxStamina);
				case 2:
					return (amount + currentThirst < maxThirst) ? (currentThirst + amount) : (currentThirst = maxThirst);
				case 3:
					float r3 = this.getDefenseInfo(1, 5) * amount / 100;
					return (r3 + currentRadiation < maxRadiation) ? (currentRadiation + r3) : (currentRadiation = maxRadiation);
				case 4:
					return (amount + currentBleeding < maxBleeding) ? (currentBleeding + amount) : (currentBleeding = maxBleeding);
				case 5:
					float r5 = this.getDefenseInfo(1, 6) * amount / 100;
					return (r5 + currentPsy < maxPsy) ? (currentPsy + r5) : (currentPsy = maxPsy);
				case 6:
					float r6 = this.getDefenseInfo(1, 4) * amount / 100;
					return (r6 + currentChemical < maxChemical) ? (currentChemical + r6) : (currentChemical = maxChemical);
			}
		} else if(!act) {
			switch(advIndex) {
			case 1: return (currentStamina - amount > 1) 	? currentStamina - amount 	: 1;
			case 2: return (currentThirst - amount > 1) 	? currentThirst - amount 	: 1;
			case 3: return (currentRadiation - amount > 1) 	? currentRadiation - amount 	: 1;
			case 4: return (currentBleeding - amount > 1) 	? currentBleeding - amount 	: 1;
			case 5: return (currentPsy - amount > 1) 	? currentPsy - amount 	: 1;
			case 6: return (currentChemical - amount > 1) 	? currentChemical - amount 	: 1;
			}
		}

		return 1;
	}	
	
	public void increaseMaxWeight(float amount) {
		this.maxWeight += amount;
		sync();
	}	
	
	public float getMaxWeight() {
		return this.maxWeight;
	}
	
	public float getOverweight() {
		return maxWeight + 10;
	}
}

/**  Defenses Numbers: 
 * 		1 - Damage Resist;   
 * 		2 - Gap Resist;    
 * 		3 - Electrical Resist;   
 *      4 - Chemical Resist;      
 *      5 - Radiation Resist;      
 *      6 - PSY Resist;     
 *      7 - Stamina Boost;   
 *      8 - Burst;
 *      
 *  Adversities Numbers
 * 		1 - Stamina;
 * 		2 - Thirst;    
 * 		3 - Radiation;   
 *      4 - Bleeding;      
 *      5 - PSY;      
 *      6 - Chemical;
 *          
 * */
