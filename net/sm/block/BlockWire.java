package net.sm.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import net.sm.registry.module.TabModule;

/**
 * Created by keelfy on 19.07.2016.
 */
public class BlockWire extends Block implements IBaseBlock{

	public BlockWire(int par1) {
		super(par1, Material.web);
		this.setCreativeTab(TabModule.tabEnvirement);
	}
	
	 public void onEntityCollidedWithBlock(World par1World, int x, int y, int z, Entity par5Entity) {
	      par1World.getBlockMetadata(x, y, z);
	      
	      if(par5Entity instanceof EntityLivingBase) {
	    	  par5Entity.setInWeb();
	    	  par5Entity.attackEntityFrom(DamageSource.generic, 4);
	      }
	 }
	
	@Override
	public Block setUnlocalizedName(String par1Str) {
		super.setUnlocalizedName(par1Str);
		return this;
	}
	
    public int getRenderType() {
    	return 1;
    }
    
    public boolean renderAsNormalBlock() {
        return false;
    }
    
    public boolean isOpaqueCube() {
    	return false;
    }
    
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1World, int par2, int par3, int par4) {
        return null;
    }
}
