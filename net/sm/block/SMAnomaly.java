package net.sm.block;

import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.sm.registry.module.TabModule;

/**
 * Created by keelfy on 24.07.2016.
 */
public class SMAnomaly extends Block {

	public static String resDir = "";
	
	public SMAnomaly(int id) {
		super(id, Material.grass);
		this.setCreativeTab(TabModule.tabAnomaly);
	}

	public static void setResourceDomain(String resdir) {
		resDir = resdir;
	}
	
	protected String getResourceDomain() {
		return resDir;
	}
	
	public boolean isOpaqueCube() {
		return false;
	}
	
	@Override
	public MovingObjectPosition collisionRayTrace(World par1World, int par2, int par3, int par4, Vec3 par5Vec3, Vec3 par6Vec3) {
		return Minecraft.getMinecraft().thePlayer.capabilities.isCreativeMode ? super.collisionRayTrace(par1World, par2, par3, par4, par5Vec3, par6Vec3) : null;
	}
	
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1World, int par2, int par3, int par4) {
	    return null;
 
    }
}
