package net.sm.block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.util.Icon;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.sm.registry.module.TabModule;

/**
 * Created by keelfy on 19.07.2016.
 */
public class SMInvicible extends Block implements IBaseBlock {

	public Icon[] iconArray;
	
	public SMInvicible(int par1) {
        super(par1, Material.rock); 
        this.setCreativeTab(TabModule.tabEnvirement);
        this.setLightOpacity(0);
    }

	@Override
	public boolean isOpaqueCube() {
		return false;
	}
   
	@Override
	public boolean shouldSideBeRendered(IBlockAccess par1IBlockAccess, int par2, int par3, int par4, int par5) {
			
		if(!Minecraft.getMinecraft().thePlayer.capabilities.isCreativeMode) 
			super.shouldSideBeRendered(par1IBlockAccess, par2, par3, par4, par5);
		
		return false;
	 }
		
	@Override
	 public MovingObjectPosition collisionRayTrace(World par1World, int par2, int par3, int par4, Vec3 par5Vec3, Vec3 par6Vec3) {
		
		return Minecraft.getMinecraft().thePlayer.capabilities.isCreativeMode ? super.collisionRayTrace(par1World, par2, par3, par4, par5Vec3, par6Vec3) : null;
	 }
		
	@Override
	public int quantityDropped(Random par1Random) {
	     return 0;
	}	
}
