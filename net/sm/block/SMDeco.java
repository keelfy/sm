package net.sm.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.sm.registry.module.TabModule;

/**
 * Created by keelfy on 19.07.2016.
 */
public class SMDeco extends Block implements IBaseBlock {

	public SMDeco(int par1, Material par2Material) {
		super(par1, par2Material);
		setCreativeTab(TabModule.tabDeco);
	}
	
    public Block setUnlocalizedName(String par1Str)
    {
    	super.setUnlocalizedName(par1Str);
        return this;
    }
}
