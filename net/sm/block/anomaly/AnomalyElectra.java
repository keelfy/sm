package net.sm.block.anomaly;

import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import net.sm.Constants;
import net.sm.block.IBaseBlock;
import net.sm.block.SMAnomaly;
import net.sm.client.fx.ElectroFX;
import net.sm.properties.SMPlayer;

/**
 * Created by keelfy on 24.07.2016.
 */
public class AnomalyElectra extends SMAnomaly implements IBaseBlock {

	private String resDir = Constants.MODID;
	private int damageValue = 0;
	
	/**
	 * @param id - Id of anomaly
	 * @param damageValue - Amount of damage that will be received by player
	 */
	public AnomalyElectra(int id, int damageValue) {
		super(id);
		//resDir = this.getResourceDomain();
		this.damageValue = damageValue;
		this.setBlockBounds(-1.0F, 0.0F, -1.0F, 2.0F, 0.1F, 2.0F);
		this.setLightValue(0.5f);
	}
	
    public void onEntityCollidedWithBlock(World par1World, int x, int y, int z, Entity par5Entity) {
    	par1World.getBlockMetadata(x, y, z);
    	Random rand =  new Random();
    	
	    if(!(par5Entity instanceof EntityPlayer))
	    	par5Entity.setDead();
	    else if(par5Entity instanceof EntityPlayer) {
	    	if(rand.nextInt(20) == 1) {
	    		par5Entity.attackEntityFrom(DamageSource.generic, damageValue);
	    		((EntityPlayer) par5Entity).addChatMessage(String.valueOf(SMPlayer.get((EntityPlayer)par5Entity).getAdversityInfo(1, 7)));
	    	}
	    	
	    	((EntityPlayer) par5Entity).jumpMovementFactor = 0;
	    }
	    	
	    
	    if(!par1World.isRemote && par1World.rand.nextFloat() > 0.95F && par5Entity instanceof EntityLivingBase && !par5Entity.isEntityInvulnerable())
	        par1World.playSoundAtEntity(par5Entity, resDir + ":kiselh", 1.0F, 1.0F);
	}
    
    @SideOnly(Side.CLIENT)
    @Override
    public void randomDisplayTick(World par1World, int par2, int par3, int par4, Random par5Random) {
    	if(par1World.rand.nextFloat() < 0.005F)
    		par1World.playSound((double)par2, (double)par3, (double)par4, resDir + ":kisel", 0.5F + par1World.rand.nextFloat() * 0.5F, 0.9F + par5Random.nextFloat() * 0.15F, false);
      
       boolean hz =  par5Random.nextBoolean();
       float rand = 0.1F;
       rand  = hz ? -0.1f : 0.1f;
       
       float f7 = (float)par2 + 0.4F + rand;
       float f2 = (float)par3 + 0.1F;
       float f3 = (float)par4 + 0.4F + rand;
       
       Minecraft.getMinecraft().effectRenderer.addEffect(new ElectroFX(par1World, (double)(f7), (double)f2, (double)(f3)));
   }
}
