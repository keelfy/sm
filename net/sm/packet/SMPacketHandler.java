package net.sm.packet;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.Player;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.sm.Constants;
import net.sm.properties.SMPlayer;

/**
 * Created by keelfy on 20.07.2016.
 */
public class SMPacketHandler implements IPacketHandler {

	public SMPacketHandler() {}

	@Override
	public void onPacketData(INetworkManager manager, Packet250CustomPayload packet, Player player)
	{
		if (packet.channel.equals(Constants.CHANNEL1)) {
			handleAdversities(packet, player);
		}
	}
	
	private void handleAdversities(Packet250CustomPayload packet, Player player)
	{
		DataInputStream inputStream = new DataInputStream(new ByteArrayInputStream(packet.data));
		
		SMPlayer props = SMPlayer.get((EntityPlayer) player);

		try {
			props.set(0, inputStream.readFloat());
			props.set(1, inputStream.readFloat());
			props.set(2, inputStream.readFloat());
			props.set(3, inputStream.readFloat());
			props.set(4, inputStream.readFloat());
			props.set(5, inputStream.readFloat());
			props.set(6, inputStream.readFloat());
			props.set(7, inputStream.readFloat());
			props.set(8, inputStream.readFloat());
			props.set(9, inputStream.readFloat());
			props.set(10, inputStream.readFloat());
			props.set(11, inputStream.readFloat());
			props.set(12, inputStream.readFloat());
			props.set(13, inputStream.readFloat());
			props.set(14, inputStream.readFloat());
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}
}
